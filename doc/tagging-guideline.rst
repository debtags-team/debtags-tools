==============================
Guideline for tagging packages
==============================

----------------------------------
Tags that should always be present
----------------------------------

* Every package should have one or more tags from ``role::``.
* Every package containing software should have one or more tags from
  ``interface::``.
* Each package containing software or libraries should have one or more tags
  from ``implemented-in::``.


----------------------------
How to add to this guideline
----------------------------

This guideline is maintained at::

  svn://svn.debian.org/debtags/debtags/trunk/doc/tagging-guideline.rst

Please send patches to::

  debtags-devel@lists.alioth.debian.org
