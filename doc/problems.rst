Debtags problems
================

The "Gimp is not an image editor" problem
-----------------------------------------

Gimp is not an image editor application: it's an image manipulation
application.  At least, according to its description.  This means that gimp
will not show up when doing ``apt-cache search image editor``.

Debtags should give users a simple way to find gimp when looking for image
editors.

This is currently solved successfully using the Smart Search algorithm:
http://debtags.alioth.debian.org/ssearch.html


The "Justin is tagging the packages" problem
--------------------------------------------

Justin is a long time Debian contributor who understands tagging really, really
well.  He does a lot of good quality tagging work.

The current state of the Debtags interfaces, however, cannot track which
changes have been made by Justin.  This means that Enrico, who reviews tag
submissions, will have to review Justin's changes together with all the other,
sometimes random, changes coming from the various submission interfaces.

Justin's submissions, and those of trusted hard working people like Justin, and
those of field experts adopting some tags, should go directly in the reviewed
tag database.

This problem is not yet solved.  There is some possibility opening in this
direction with the new website backend has prototype support for recording tag
patches: http://lists.alioth.debian.org/pipermail/debtags-devel/2006-November/001394.html


The "Exotic compiler" problem
-----------------------------

Tagging what is the language supported by a development tool seems like a
simple task: ``gcc`` can be tagged ``devel::lang:c, devel::lang:c++`` and
``perl`` can be tagged ``devel::lang:perl``.

After some time tagging, one realises that we have a constant influx of all
sorts of development tools for more or less minor languages.  Here is an
example, incomplete list: ``aleph``, ``A+``, ``aspectc++``, ``boo``,
``brainfuck``, ``cduce``, ``chuck``, ``confluence``, all the various dialects
of Lisp/Scheme and those of ML, ``faust``, ``gambas``, ``goo``, ``gvrng``,
``haxe``, ``kaya``, ``lisaac``, ``mercury``, ``mozart``, ``oberon2``,
``scala``, ``slate``, ``slice``, ``crm114``, ``spl``, ``tela``, ``u++``,
``unlambda``, ``whitespace``, ``yorick``, ``zinc``, ``zoem``.

After noticing one or two of those packages, one thinks that it is ok to create
a new tag for every language that enters Debian, but when one realises how long
the list can get, one realises that doing so would lead to confusing the
Debtags language list for very little benefit.

Then one is tempted to make a tag devel::lang:exotic that encompasses "all the
weird ones".  But then who decides if a language is exotic?  Would Pascal be
exotic, for example?  And Forth?  And Postscript?

Both these two solutions produce more problems than they solve.

The best solution found so far is doing nothing, and use the other tags we have
to create the best approximate tagging.  We do have ``devel::interpreter`` and
``devel::compiler`` to begin with.  Some packages such as ``whitespace`` can
also use ``use::entertaining``; ``chuck`` can use ``works-with::audio``;
``tela`` and ``yorick`` can use ``field::mathematics``.

This shows the amazing expressivity of faceted categorisation: when you don't
have a precise tag for a new concept, you have many ways to approximate it and
you still get a good level of completeness.

After this reasoning on exotic programming languages, we realise that the
programming language itself may not be the main aspect of the package, but
rather the main field in which the language is useful, or the special kind of
data the language works with.  When more packages will appear all working with
a programming language, the programming language itself will become more
relevant and a new useful tag can finally be created.


Reference: http://lists.alioth.debian.org/pipermail/debtags-devel/2006-November/001380.html

